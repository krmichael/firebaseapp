import React, {Component} from 'react';
import {
  View,
  TouchableHighlight,
  Text,
  YellowBox
} from 'react-native';

import firebase from 'firebase';

YellowBox.ignoreWarnings(['Setting a timer']);

export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      pontos: 0,
      msgError: ''
    }
  }

  componentWillMount() {
    
    const config = {
      apiKey: "YOUR-APP-KEY",
      authDomain: "YOUR-DOMAIN-FIREBASE",
      databaseURL: "YOUR-URL-DATABASE",
      projectId: "ID-PROJECT",
      storageBucket: "",
      messagingSenderId: "MESSAGE-SEND-ID"
    };
    
    if(!firebase.apps.length) {
      firebase.initializeApp(config);
    }
  }

  salvarDados = () => {
    var funcionarios = firebase.database().ref('funcionarios');
        funcionarios.push()
                    .set({
                      nome: 'Karla Fonseca',
                      email: 'karla@k.com',
                      cidade: 'Unai MG'
                    });
  }

  listarDados = () => {
    var pontuacao = firebase.database().ref('pontuacao');
        pontuacao.on('value', (snapshot) => {
          this.setState({ pontos: snapshot.val() });
        });
  }

  cadastrarUsuario = () => {
    
    const User = {
      email: 'lean@lean.com',
      password: 'lean123456'
    }

    firebase.auth()
            .createUserWithEmailAndPassword(User.email, User.password)
            .catch((err) => {
              let message = err.message;

              if(err.code === 'auth/weak-password') {
                message = 'A senha deve ter pelo menos 6 caracteres';
              }

              this.setState({ msgError: message });
            });
  }

  verificarUsuarioLogado = () => {
    
    firebase.auth()
            .onAuthStateChanged((user) => user ?
            alert('Usuário logado!') :
            alert('Usuário não logado!'));

  }

  deslogarUsuario = () => {
    firebase.auth().signOut();
  }

  logarUsuario = () => {
    
    const User = {
      email: 'lean@lean.com',
      password: 'lean123456'
    }

    firebase.auth()
            .signInWithEmailAndPassword(User.email, User.password)
            .catch((err) => {
              let message = 'Dados inválidos. Tente novamente!';
              this.setState({ msgError: message });
            });
  }
  
  render() {
    const { pontos, msgError } = this.state;
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <TouchableHighlight onPress={() => {this.salvarDados()}} accessibilityLabel='Salvar dados'>
          <Text
            style={{
              color: '#841584',
              backgroundColor: '#ddd',
              padding: 10,
              borderRadius: 4
            }}>Salvar dados</Text>
        </TouchableHighlight>

        <View style={{marginTop: 10}}>
          <TouchableHighlight onPress={() => {this.listarDados()}} accessibilityLabel='Listar dados'>
            <Text
              style={{
                color: '#841584',
                backgroundColor: '#ddd',
                padding: 10,
                borderRadius: 4
              }}>Listar dados</Text>
          </TouchableHighlight>
        </View>

        <View style={{marginTop: 10}}>
          <Text style={{textAlign: 'center', color: '#841584'}}>Pontos: { pontos }</Text>
        </View>

        <View style={{marginTop: 10}}>
          <TouchableHighlight onPress={() => {this.cadastrarUsuario()}} accessibilityLabel='Cadastrar Usuario'>
            <Text
              style={{
                color: '#841584',
                backgroundColor: '#ddd',
                padding: 10,
                borderRadius: 4
              }}>Cadastrar Usuario</Text>
          </TouchableHighlight>
        </View>
        <View style={{marginTop: 10}}>
          <Text style={{textAlign: 'center', color: '#841584'}}>{msgError}</Text>
        </View>

        <View style={{marginTop: 10}}>
          <TouchableHighlight onPress={() => {this.verificarUsuarioLogado()}} accessibilityLabel='Verificar Usuario Logado'>
            <Text
              style={{
                color: '#841584',
                backgroundColor: '#ddd',
                padding: 10,
                borderRadius: 4
              }}>Verificar Usuario Logado</Text>
          </TouchableHighlight>
        </View>

        <View style={{marginTop: 10}}>
          <TouchableHighlight onPress={() => {this.deslogarUsuario()}} accessibilityLabel='Deslogar Usuario'>
            <Text
              style={{
                color: '#841584',
                backgroundColor: '#ddd',
                padding: 10,
                borderRadius: 4
              }}>Deslogar Usuario</Text>
          </TouchableHighlight>
        </View>

       <View style={{marginTop: 10}}>
          <TouchableHighlight onPress={() => {this.logarUsuario()}} accessibilityLabel='Logar Usuario'>
            <Text
              style={{
                color: '#841584',
                backgroundColor: '#ddd',
                padding: 10,
                borderRadius: 4
              }}>Logar Usuario</Text>
          </TouchableHighlight>
        </View>

      </View>
    );
  }
}